from pathlib import Path
import subprocess


class Key:
    def __init__(
        self,
        file_name: str,
        category: str,
        name: str,
        algo: str,
        pubkey: str,
    ):
        self.pub_file_name = file_name
        self.priv_file_name = file_name[0:-4]
        self.category = category
        self.name = name
        self.algo = algo
        self.pubkey = pubkey
        self.loaded = False

    def match(self, algo, pubkey):
        if self.algo == algo and self.pubkey == pubkey:
            self.loaded = True

    def unload(self):
        if self.loaded:
            subprocess.run(
                ["ssh-add", "-d", f"{self._ssh_dir}/{self.priv_file_name}"],
                check=False,
                capture_output=True,
            )
        self.loaded = False

    def load(self):
        if not self.loaded:
            subprocess.run(
                ["ssh-add", f"{self._ssh_dir}/{self.priv_file_name}"],
                capture_output=True,
            )
        self.loaded = True

    def __lt__(self, o):
        if self.category < o.category:
            return True
        elif self.category > o.category:
            return False
        elif self.name < o.name:
            return True
        else:
            return False

    @property
    def _ssh_dir(self):
        return Path.home() / ".ssh"


if __name__ == "__main__":
    try:
        import inquirer
    except ModuleNotFoundError:
        import sys

        print("Cannot run kees in a virtual environment", file=sys.stdout)
        exit(1)

    ssh_dir = Path.home() / ".ssh"
    keys = []
    for file in ssh_dir.glob("*.pub"):
        try:
            category, name = file.name[:-4].split("_", maxsplit=2)
            if category == "id":
                category = "<general>"
        except ValueError:
            category = "<general>"
            name = file.name[:-4]
        algo, pubkey, *rest = file.read_text().split(" ")
        keys.append(Key(file.name, category, name, algo, pubkey))

    keys.sort()

    loaded_keys = []
    try:
        loaded_keys = [
            line.split(" ")[0:2]
            for line in (subprocess.check_output(["ssh-add", "-L"]))
            .decode("utf-8")
            .split("\n")
            if line
        ]
    except subprocess.CalledProcessError:
        pass

    for algo, pubkey in loaded_keys:
        for key in keys:
            key.match(algo, pubkey)

    KEY_KEY = "keys"
    while True:
        questions = [
            inquirer.Checkbox(
                KEY_KEY,
                message="Which would you like to load?",
                choices=[
                    (f"{key.category}: {key.name}", key.pub_file_name) for key in keys
                ],
                default=[key.pub_file_name for key in keys if key.loaded],
            )
        ]
        answers = inquirer.prompt(questions)
        to_load = []
        if answers:
            for file_name in answers[KEY_KEY]:
                for key in keys:
                    if key.pub_file_name == file_name:
                        to_load.append(key)

            to_unload = []
            for key in keys:
                if key not in to_load:
                    to_unload.append(key)

            for key in to_unload:
                key.unload()
            for key in to_load:
                key.load()
            break
